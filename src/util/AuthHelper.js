import axios from 'axios'

// export const authHelper = {
//     authHeader,
//     login,
//     logout
// }

export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

export async function login(email, password) {
    const requestData = {
        email,
        password
    }

    const apihosturl = process.env.VUE_APP_URL
    const response = await axios.post(apihosturl+'/login', requestData)
    const user = response.data
    // login successful if there's a jwt token in the response        
    if (user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
    }
    
    return user;
}

export function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user')
}