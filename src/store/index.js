import Vuex from 'vuex'
import Vue from 'vue'
import AppLogin from './modules/AppLogin'
import AppItems from './modules/AppItems'
import AppCategory from './modules/AppCategory'
import {alert} from './modules/AppAlert'

//load Vuex
Vue.use(Vuex)

//Create store
export default new Vuex.Store({
    modules:{
        AppLogin,
        AppItems,
        AppCategory,
        alert
    }
})