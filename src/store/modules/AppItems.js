import axios from 'axios'

const state = {
    trending:{},
    recommendation:{},
    recentlyViewed:{}
}

const getters = {
    trending: (state) => state.trending,
    recommendation: (state) => state.recommendation,
    recentlyViewed: (state) => state.recentlyViewed
}

const actions = {
    async fetchTrending({ commit }) {
        try {
            const apihosturl = process.env.VUE_APP_URL
            const response = await axios.get(apihosturl+'/items');
            console.log(response);
            const data = {
                title:"Trending",
                items:response.data
            }
            commit('setTrending',data)
          } catch (error) {
            console.error(error);
          }
        
    },
    async fetchRecommendation({ commit }) {
        try {
            const apihosturl = process.env.VUE_APP_URL
            const response = await axios.get(apihosturl+'/items');
            console.log(response);
            const data = {
                title:"Recommendation",
                items:response.data
            }
            commit('setRecommendation',data)
          } catch (error) {
            console.error(error);
          }
        
    },
    async fetchRecentlyViewed({ commit }) {
        try {
            const apihosturl = process.env.VUE_APP_URL
            const response = await axios.get(apihosturl+'/items');
            console.log(response);
            const data = {
                title:"Recently Viewed",
                items:response.data
            }
            commit('setRecentlyViewed',data)
          } catch (error) {
            console.error(error);
          }
        
    }

}

const mutations = {
    setTrending: (state, trending) => (state.trending = trending),
    setRecommendation: (state, recommendation) => (state.recommendation = recommendation),
    setRecentlyViewed: (state, recentlyViewed) => (state.recentlyViewed = recentlyViewed)
}

export default {
    state,
    getters,
    actions,
    mutations
}