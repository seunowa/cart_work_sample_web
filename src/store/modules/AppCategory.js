import axios from 'axios'

const state = {
    categories:[]
}

const getters = {
    categories: (state) => state.categories
}

const actions = {
    async fetchCategories({ commit }) {
        try {
            const apihosturl = process.env.VUE_APP_URL
            const response = await axios.get(apihosturl+'/categories')
            commit('setCategories',response.data)
        } catch (error) {
            console.error(error);
        }          
    }

}

const mutations = {
    setCategories: (state, categories) => (state.categories = categories)
}

export default {
    state,
    getters,
    actions,
    mutations
}