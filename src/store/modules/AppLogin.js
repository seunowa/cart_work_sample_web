import * as AuthHelper from '../../util/AuthHelper.js'
import {router} from '../../main.js'

const user = JSON.parse(localStorage.getItem('user'));
const state = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };

const getters = {
    jwt: (state) => state.jwt
}

const actions = {
    async login({ dispatch, commit }, { email, password, url}) {
        commit('loginRequest', { email });
        
        const user = await AuthHelper.login(email, password)
        console.log(user)
        if(user.token){
            commit('loginSuccess', user)
            if(url){
                router.push(url)
            }else{
                router.push('/')
            }
        }else{
            commit('loginFailure', user)
            dispatch('alert/error', user, {root:true})
        }
    },
    logout({ commit }){
        AuthHelper.logout()
        commit('logout')
        router.push('/') //navigate to home page on logout
    }

}

const mutations = {
    setJwt: (state, jwt) => (state.jwt = jwt),
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}