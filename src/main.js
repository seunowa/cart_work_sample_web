import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import  Routes from './routes'
import store from './store'

Vue.use(VueRouter)

export const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const protectedPages = ['/cart', '/orders'];
  const authRequired = protectedPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/signin');
  }

  next();
})

Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  render: h => h(App),
  router: router
}).$mount('#app')
