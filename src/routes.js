import AppHome from './components/AppHome.vue'
import AppSearchResult from './components/AppSearchResult.vue'
import AppCart from './components/AppCart.vue'
import AppItemDetail from './components/AppItemDetail.vue'
import AppSignin from './components/AppSignin.vue'
import AppOrders from './components/AppOrders.vue'
import AppRegister from './components/AppRegister.vue'

export default [
    {path:'/', component:AppHome},
    {path:'/search', component:AppSearchResult},
    {path:'/cart', component:AppCart},
    {path:'/items/:id', component:AppItemDetail},
    {path:'/signin', component:AppSignin},
    {path:'/orders', component:AppOrders},
    {path:'/register', component:AppRegister},

    // otherwise redirect to home
    { path: '*', redirect: '/' }
]